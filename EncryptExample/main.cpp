#include <iostream>

#include "EncryptFile.hpp"
#include "RSA.hpp"
#include "ElGamal.hpp"

using namespace std;

typedef enum { CODING, DECODING } DirectionEncryption;

int main()
{
	DirectionEncryption directEncrypt;
	EncryptMethod currentMethod;
	string nameInputFile, nameOutputFile;
	int32_t userChoice;

	cout << "1. ElGamal" << endl << "2. RSA" << endl;
	cin >> userChoice;
	currentMethod = (userChoice == 1)? ELGAMAL_METHOD: RSA_METHOD;

	cout << "1. Coding" << endl << "2. Decoding" << endl;
	cin >> userChoice;
	directEncrypt = (userChoice == 1)? CODING: DECODING;

	cout << "Input input file name:";
	cin >> nameInputFile;
	cout << "Input output file name:";
	cin >> nameOutputFile;


	EncryptFile encrypt((currentMethod == ELGAMAL_METHOD)? shared_ptr<AbstractEncryptMethod>(new ElGamal()): shared_ptr<AbstractEncryptMethod>(new RSA()));

	if (directEncrypt == CODING) {
		encrypt.codingFile(nameInputFile, nameOutputFile);
	} else {
		encrypt.decodingFile(nameInputFile, nameOutputFile);
	}

	return 0;
}
