#include <iostream>

#include "Crypto.hpp"

using namespace std;

int main()
{
	uint32_t q, p, g, xa, xb, ya, yb, zab, zba, k = 32;

	do{
		q = Crypto::GeneratePrimeNumber(k - 1);
	} while (!Crypto::IsPrimeNumber(p = 2 * q + 1));

	do {
		g = Crypto::GeneratePrimeNumber(k - 1);
	} while (Crypto::QuickPow(g, q, p) == 1);

	xa = Crypto::GeneratePrimeNumber(k - 1);
	xb = Crypto::GeneratePrimeNumber(k - 1);
	ya = Crypto::QuickPow(g, xa, p);
	yb = Crypto::QuickPow(g, xb, p);

	zab = Crypto::QuickPow(yb, xa, p);
	zba = Crypto::QuickPow(ya, xb, p);

	if (zab == zba)
		cout << "WIN" << endl;
	else
		cout << "EPIC FAIL" << endl;
	return 0;
}
