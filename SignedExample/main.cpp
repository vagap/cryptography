#include <iostream>
#include <fstream>

#include "SignedFile.hpp"
#include "RSA.hpp"
#include "ElGamal.hpp"

using namespace std;

typedef enum { SIGNED, CHECK } ChoiseAction;

int main()
{
	ChoiseAction action;
	EncryptMethod currentMethod;
	string fileName;
	int32_t userChoice;

	cout << "1. ElGamal" << endl << "2. RSA" << endl;
	cin >> userChoice;
	currentMethod = (userChoice == 1)? ELGAMAL_METHOD: RSA_METHOD;

	cout << "1. Signed" << endl << "2. Check sign" << endl;
	cin >> userChoice;
	action = (userChoice == 1)? SIGNED: CHECK;

	cout << "Input input file name:";
	cin >> fileName;

	SignedFile Signed((currentMethod == ELGAMAL_METHOD)? shared_ptr<AbstractEncryptMethod>(new ElGamal()): shared_ptr<AbstractEncryptMethod>(new RSA()));

	if (action == SIGNED)
		Signed.getSigned(fileName);
	else {
		if (Signed.isCheckSign(fileName))
			cout << "True file" << endl;
		else
			cout << "It's a FAKE" << endl;
	}
	return 0;
}
