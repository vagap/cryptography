#include <map>
#include <cmath>
#include <iostream>

#include "Crypto.hpp"

using namespace std;

int main()
{
	uint32_t a, y, p, m, am, aim;
	map<uint32_t, uint32_t> GiantSteps, BabySteps;

	m = ceil(sqrt(p));

	cout << "a: ";
	cin >> a;
	cout << "y: ";
	cin >> y;
	cout << "p: ";
	cin >> p;

	uint32_t ai = a;

	BabySteps.insert(pair<uint32_t, uint32_t>(y, 0u));
	GiantSteps.insert(pair<uint32_t, uint32_t>(aim = am = Crypto::QuickPow(a, m, p), 1u));
	for (uint32_t i = 1; i < m; ++i) {
		ai = ((uint64_t)a * (uint64_t)ai) % (uint64_t)p;
		BabySteps.insert( pair<uint32_t, uint32_t>(((uint64_t)y * (uint64_t)ai) % (uint64_t)p, i) );
		GiantSteps.insert( pair<uint32_t, uint32_t>(aim = (((uint64_t)aim * (uint64_t)am) % (uint64_t)p), i + 1) );
	}

	map<uint32_t, uint32_t>::iterator BabyStep = BabySteps.begin(), GiantStep = GiantSteps.begin();

	while ((BabyStep != BabySteps.end()) && (GiantStep != GiantSteps.end()))
	{
		if (BabyStep->first == GiantStep->first) {
			uint32_t x = GiantStep->second * m - BabyStep->second;
			cout << "x = " << x << endl;
			cout << Crypto::QuickPow(a, x, p) << endl;
			++BabyStep;
			++GiantStep;
		} else {
			if (BabyStep->first < GiantStep->first)
				++BabyStep;
			else
				++GiantStep;
		}
	}

    return 0;
}
