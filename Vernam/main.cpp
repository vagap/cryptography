#include <iostream>
#include <fstream>

#include "Crypto.hpp"

using namespace std;

static char keyFileName[] = "VernamKey";

int main()
{
	ifstream inFile;
	ofstream outFile;
	fstream keyFile;
	string nameInputFile, nameOutputFile;
	DirectionEncryption directEncrypt;
	int32_t userChoice;

	cout << "1. Coding" << endl << "2. Decoding" << endl;
	cin >> userChoice;
	directEncrypt = (userChoice == 1)? CODING: DECODING;
	cout << "Input input file name:";
	cin >> nameInputFile;
	cout << "Input output file name:";
	cin >> nameOutputFile;

	inFile.open(nameInputFile.c_str(), ios::binary);
	outFile.open(nameOutputFile.c_str(), ios::binary);
	if (directEncrypt == CODING)
		keyFile.open(keyFileName, ios::out | ios::binary);
	else
		keyFile.open(keyFileName, ios::in | ios::binary);

	char buffer, keyBuffer;
	while (!inFile.eof()) {
		buffer = inFile.get();
		if (!inFile.good())
			break;
		if (directEncrypt == CODING) {
			keyBuffer = Crypto::GenerateNumber();
			keyFile.write(&keyBuffer, 1);
		} else
			keyBuffer = keyFile.get();
		buffer ^= keyBuffer;
		outFile.write(&buffer, 1);
	}

	return 0;
}
