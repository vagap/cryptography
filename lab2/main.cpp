#include <iostream>

#include "Crypto.hpp"

using namespace std;

int main()
{
	int a, b;
	cout << "a:";
	cin >> a;
	cout << "b:";
	cin >> b;
    cout << "Result: " << Crypto::GreatestCommonDivisor(a, b) << endl;
    return 0;
}
