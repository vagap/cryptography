#include <iostream>

#include "Crypto.hpp"

using namespace std;

int main()
{
	int32_t p, ca, da, cb, db, m, x1, x2, x3, x4;

	p = Crypto::GeneratePrimeNumber(14);
	//p = 23;
	do {
		ca = Crypto::GeneratePrimeNumber(14) * Crypto::GeneratePrimeNumber(14);
		//ca = 7;
	} while ((Crypto::GreatestCommonDivisor(ca, p - 1, da) != 1) || (da < 0));
	do {
		cb = Crypto::GeneratePrimeNumber(14) * Crypto::GeneratePrimeNumber(14);
		//cb = 5;
	} while ((Crypto::GreatestCommonDivisor(cb, p - 1, db) != 1) || (db < 0));

	cout << "p: " << p << endl;
	cout << "da: " << da << endl;
	cout << "db: " << db << endl;
	cout << "m: ";
	cin >> m;

	x1 = Crypto::QuickPow(m, ca, p);
	x2 = Crypto::QuickPow(x1, cb, p);
	x3 = Crypto::QuickPow(x2, da, p);
	x4 = Crypto::QuickPow(x3, db, p);

	if (m == x4)
		cout << "WIN" << endl;
	else
		cout << "EPIC FAIL" << endl;
	return 0;
}
