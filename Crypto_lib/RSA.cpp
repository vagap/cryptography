#include "RSA.hpp"

RSA::RSA():
	m_c(0), m_d(65537), m_n(0)
{
	uint32_t q, p, fi;

	do {
		q = Crypto::GeneratePrimeNumber(16);
		p = Crypto::GeneratePrimeNumber(16);

		m_n = p * q;
		fi = (p - 1) * (q - 1);

		Crypto::GreatestCommonDivisor(m_d, fi, m_c);
	} while (m_c < 0);
}

uint32_t RSA::coding(uint32_t data)
{
	return Crypto::QuickPow(data, m_d, m_n);
}

uint32_t RSA::decoding(uint32_t data)
{
	return Crypto::QuickPow(data, m_c, m_n);
}

void RSA::writePrivateKey(std::ostream &keyStream)
{
	keyStream.write((char *) &m_c, sizeof(m_c));
	keyStream.write((char *) &m_n, sizeof(m_n));
}

void RSA::readPrivateKey(std::istream &keyStream)
{
	keyStream.read((char *) &m_c, sizeof(m_c));
	keyStream.read((char *) &m_n, sizeof(m_n));
}

void RSA::writePublicKey(std::ostream &keyStream)
{
	keyStream.write((char *) &m_d, sizeof(m_d));
	keyStream.write((char *) &m_n, sizeof(m_n));
}

void RSA::readPublicKey(std::istream &keyStream)
{
	keyStream.read((char *) &m_d, sizeof(m_d));
	keyStream.read((char *) &m_n, sizeof(m_n));
}
