#include "SHA.hpp"
#include<iostream>

const word SHA256::ROUND_CONSTANTS[EXTENDED_WORD_AMOUNT] =
{
	0x428a2f98ULL, 0x71374491ULL, 0xb5c0fbcfULL, 0xe9b5dba5ULL, 0x3956c25bULL, 0x59f111f1ULL, 0x923f82a4ULL, 0xab1c5ed5ULL,
	0xd807aa98ULL, 0x12835b01ULL, 0x243185beULL, 0x550c7dc3ULL, 0x72be5d74ULL, 0x80deb1feULL, 0x9bdc06a7ULL, 0xc19bf174ULL,
	0xe49b69c1ULL, 0xefbe4786ULL, 0x0fc19dc6ULL, 0x240ca1ccULL, 0x2de92c6fULL, 0x4a7484aaULL, 0x5cb0a9dcULL, 0x76f988daULL,
	0x983e5152ULL, 0xa831c66dULL, 0xb00327c8ULL, 0xbf597fc7ULL, 0xc6e00bf3ULL, 0xd5a79147ULL, 0x06ca6351ULL, 0x14292967ULL,
	0x27b70a85ULL, 0x2e1b2138ULL, 0x4d2c6dfcULL, 0x53380d13ULL, 0x650a7354ULL, 0x766a0abbULL, 0x81c2c92eULL, 0x92722c85ULL,
	0xa2bfe8a1ULL, 0xa81a664bULL, 0xc24b8b70ULL, 0xc76c51a3ULL, 0xd192e819ULL, 0xd6990624ULL, 0xf40e3585ULL, 0x106aa070ULL,
	0x19a4c116ULL, 0x1e376c08ULL, 0x2748774cULL, 0x34b0bcb5ULL, 0x391c0cb3ULL, 0x4ed8aa4aULL, 0x5b9cca4fULL, 0x682e6ff3ULL,
	0x748f82eeULL, 0x78a5636fULL, 0x84c87814ULL, 0x8cc70208ULL, 0x90befffaULL, 0xa4506cebULL, 0xbef9a3f7ULL, 0xc67178f2ULL
	// These values are round constants, based on the first 32 bits of the fractional parts of the
	// cube roots of the first 64 prime numbers.
};

const word SHA256::INITIAL_VALUES[SIZE_H_ARRAY] =
{
	0x6a09e667ULL, // These values are used to
	0xbb67ae85ULL, // initialize the chunks of the hash.
	0x3c6ef372ULL, // They are based on the first 32 bits
	0xa54ff53aULL, // of the fractional parts of the square roots
	0x510e527fULL, // of the first 8 prime numbers.
	0x9b05688cULL,
	0x1f83d9abULL,
	0x5be0cd19ULL
};

SHA256::SHA256():
	m_h(),
	m_positionFillWord(0),
	m_positionFillByte(WORD_BYTES - 1),
	m_SizeData(0)
{
	for (uint32_t i = 0; i < SIZE_H_ARRAY; ++i)
		m_h.push_back(INITIAL_VALUES[i]);
}

void SHA256::update(const byte *buffer, size_t bufferSize)
{
	static byte *chunkBuffer = (byte *)&m_ChunkBuffer;
	for (uint32_t i = 0; i < bufferSize; ++i)
	{
		chunkBuffer[m_positionFillWord * WORD_BYTES + m_positionFillByte] = buffer[i];
		if (m_positionFillByte != 0)
			--m_positionFillByte;
		else {
            m_positionFillByte = WORD_BYTES - 1;
            if (++m_positionFillWord == SIZE_FILL_DATA_CHUNK) {
				processFillChunk();
				m_positionFillWord = 0;
            }
		}
	}
	m_SizeData += bufferSize;
}

void SHA256::finish(std::vector<word> &result)
{
	finishFillChunk();

	for (uint32_t i = 0; i < SIZE_H_ARRAY; ++i)
		result.push_back(m_h[i]);
}

void SHA256::processFillChunk()
{
	word s0, s1;
	for(int i = 16; i < EXTENDED_WORD_AMOUNT; i++)
	{
		word w0 = m_ChunkBuffer[i - 15],
				w1 = m_ChunkBuffer[i - 2];

		s0 = (rightRotate(w0, 7)) ^ (rightRotate(w0, 18)) ^ (w0 >> 3);
		s1 = (rightRotate(w1, 17)) ^ (rightRotate(w1, 19)) ^ (w1 >> 10);

		m_ChunkBuffer[i] = m_ChunkBuffer[i - 16] + s0 + m_ChunkBuffer[i - 7] + s1;
	}

	word a = m_h[0], b = m_h[1], c = m_h[2], d = m_h[3], e = m_h[4], f = m_h[5], g = m_h[6], h = m_h[7];

	// Go into main loop
	for(int i = 0; i < EXTENDED_WORD_AMOUNT; i++)
	{
		s0 = (rightRotate(a, 2)) ^ (rightRotate(a, 13)) ^ (rightRotate(a, 22));
		word maj = (a & b) ^ (a & c) ^ (b & c);
		word t2 = s0 + maj;
		s1 = (rightRotate(e, 6)) ^ (rightRotate(e, 11)) ^ (rightRotate(e, 25));
		word ch = (e & f) ^ ((~e) & g);
		word t1 = h + s1 + ch + ROUND_CONSTANTS[i] + m_ChunkBuffer[i];

		h = g;
		g = f;
		f = e;
		e = d + t1;
		d = c;
		c = b;
		b = a;
		a = t1 + t2;
	}

	// Add this chunk's hash to the overall result
	m_h[0] += a;
	m_h[1] += b;
	m_h[2] += c;
	m_h[3] += d;
	m_h[4] += e;
	m_h[5] += f;
	m_h[6] += g;
	m_h[7] += h;
}

void SHA256::finishFillChunk()
{
	const byte INITIAL_PADDING = 0x80, // A 1 bit followed by 7 0 bits.
				EXTENDED_PADDING = 0x00;
	longword lengthData = m_SizeData * BYTE_SIZE;

	update(&INITIAL_PADDING, 1);

	word lengthExtended = 64ULL - (m_SizeData + sizeof(m_SizeData) % 64ULL);
	for (word i = 0; i < lengthExtended; ++i)
		update(&EXTENDED_PADDING, 1);

	for(int i = LONGWORD_BYTES - 1; i >= 0; i--)
	{
		// Add bytes of inputLength to input, MSB first.
		byte b = (lengthData >> i * BYTE_SIZE) & FULL_BYTE;
		update(&b, 1);
	}
}

word SHA256::rightRotate(word w, unsigned int n)
{
	// Rotates the word to the right by n bits, wrapping around to the left
	// if the end is reached. Returns the result.

	return ((w >> n) | (w << (WORD_SIZE - n)));
}

void SHA256::hash(std::istream &stream, std::vector<word> &result)
{
	byte buffer;
	while (!stream.eof()) {
		stream.read((char *) &buffer, 1);
		if (!stream.good())
			break;
		update(&buffer, 1);
	}
	finish(result);
}
