#pragma once

#include "AbstractEncryptMethod.hpp"

class ElGamal: public AbstractEncryptMethod
{
private:
    uint32_t m_p, m_cb, m_db, m_k, m_r;
public:
	ElGamal();

	virtual uint32_t coding(uint32_t data);
	virtual uint32_t decoding(uint32_t data);

	virtual void writePrivateKey(std::ostream &keyStream);
	virtual void readPrivateKey(std::istream &keyStream);
	virtual void writePublicKey(std::ostream &keyStream);
	virtual void readPublicKey(std::istream &keyStream);
};
