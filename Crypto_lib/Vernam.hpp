#pragma once

#include "EncryptFile.hpp"

class Vernam: public EncryptMethod
{
private:
	uint32_t m_key;
public:
	Vernam();
	Vernam(const char fileName[]);

	virtual uint32_t coding(uint32_t data);
	virtual uint32_t decoding(uint32_t data);

	virtual void writeSecretKey(std::ofstream &keyStream);
	virtual void writeOpenKey(std::ofstream &keyStream) { };
	virtual void readSecretKey(std::ifstream &keyStream);
	virtual void readOpenKey(std::ifstream &keyStream) { };
};
