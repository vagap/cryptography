#include "EncryptFile.hpp"

const std::string EncryptFile::FilePublicKey = "Public.Key";
const std::string EncryptFile::FilePrivateKey = "Private.Key";

EncryptFile::EncryptFile(std::shared_ptr<AbstractEncryptMethod> Encrypt):
	EncryptStream::EncryptStream(Encrypt)
{
}

void EncryptFile::codingFile(const std::string &inFileName, const std::string &outFileName)
{
	std::ofstream privateKey(FilePrivateKey, std::ios::binary), publicKey(FilePublicKey, std::ios::binary);
	writePrivateKey(privateKey);
	writePublicKey(publicKey);

	std::ifstream inFile(inFileName, std::ios::binary);
	std::ofstream outFile(outFileName, std::ios::binary);
	codingStream(inFile, outFile);
}

void EncryptFile::decodingFile(const std::string &inFileName, const std::string &outFileName)
{
	std::ifstream privateKey(FilePrivateKey, std::ios::binary), publicKey(FilePublicKey, std::ios::binary);
	readPrivateKey(privateKey);
	readPublicKey(publicKey);

	std::ifstream inFile(inFileName, std::ios::binary);
	std::ofstream outFile(outFileName, std::ios::binary);
	decodingStream(inFile, outFile);
}
