#pragma once

#include <fstream>
#include "Crypto.hpp"

class AbstractEncryptMethod
{
public:
	AbstractEncryptMethod() { };
	AbstractEncryptMethod(const char fileName[]) { };
	virtual ~AbstractEncryptMethod() { };

	virtual uint32_t coding(uint32_t data) = 0;
	virtual uint32_t decoding(uint32_t data) = 0;

	virtual void writePrivateKey(std::ostream &keyStream) = 0;
	virtual void readPrivateKey(std::istream &keyStream) = 0;
	virtual void writePublicKey(std::ostream &keyStream) = 0;
	virtual void readPublicKey(std::istream &keyStream) = 0;
};
