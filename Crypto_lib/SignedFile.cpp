#include "SignedFile.hpp"
#include "SHA.hpp"

#include <vector>
#include <iostream>

using namespace std;

const std::string SignedFile::FilePublicKey = "Public.Key";

SignedFile::SignedFile(std::shared_ptr<AbstractEncryptMethod> Encrypt):
	EncryptStream::EncryptStream(Encrypt)
{

}

void SignedFile::getSHA(const std::string &signedFileName, std::stringstream &hashStream)
{
	SHA256 sha;
	std::ifstream signedFile(signedFileName, std::ios::binary);

	std::vector<word> hashSHA;
	sha.hash(signedFile, hashSHA);

	hashStream.unsetf(std::ios::dec);
	hashStream.setf(std::ios::hex);
	for (word w: hashSHA)
		hashStream << w;
}

void SignedFile::getSigned(const std::string &signedFileName)
{
	std::ofstream publicKey(FilePublicKey, std::ios::binary);
	writePublicKey(publicKey);

	std::stringstream hashSHA;
	getSHA(signedFileName, hashSHA);

	std::ofstream outFile("sign", std::ios::binary);
	codingStream(hashSHA, outFile);
}

bool SignedFile::isCheckSign(const std::string &signedFileName)
{
	std::ifstream publicKey(FilePublicKey, std::ios::binary);
	readPublicKey(publicKey);

	std::stringstream hashSHA;
	getSHA(signedFileName, hashSHA);

	std::stringstream codingHashSHA;
	codingStream(hashSHA, codingHashSHA);

	std::ifstream signedFile("sign", std::ios::binary);

	char buffer1 = 0, buffer2 = 0;
	while (!signedFile.eof()) {
		signedFile.read(&buffer1, 1);
		codingHashSHA.read(&buffer2, 1);
		if (signedFile.eof())
			break;
		if (codingHashSHA.eof())
			return false;
		if (buffer1 != buffer2)
			return false;
	}

	return true;
}
