#pragma once

/*
	Взято с http://www.cyberforum.ru/cpp-builder/thread159050.html
*/

#include <iostream>
#include <vector>
#include <stdint.h>

typedef uint8_t  byte;
typedef uint32_t word;
typedef uint64_t longword;

class SHA256
{
protected:
	static const uint16_t	EXTENDED_WORD_AMOUNT	= 64,
							DIGEST_CHARS			= 64,
							WORD_BYTES				= 4,
							LONGWORD_BYTES			= 8,
							CHUNK_BYTES				= 64,
							BYTE_SIZE				= 8, // 8 Bits.
							SIZE_H_ARRAY			= 8,
							SIZE_FILL_DATA_CHUNK	= 16,
							WORD_SIZE				= WORD_BYTES * BYTE_SIZE, // 32 bits.
							LONGWORD_SIZE			= LONGWORD_BYTES * BYTE_SIZE; // 64 bits.
	static const byte	FULL_BYTE = 0xFF, // A byte with all bits set to 1.
						HALF_BYTE = 0x0F; // A byte with the 4 left bits set to 0 and the 4 right bits set to 1.
	static const word	ROUND_CONSTANTS[EXTENDED_WORD_AMOUNT],
						INITIAL_VALUES[SIZE_H_ARRAY];
	static const int16_t	OUT_OF_BOUNDS_E			= 1,
							INVALID_HASH_LENGTH_E	= 2,
							INVALID_CHUNK_SIZE_E	= 3;

	std::vector<word> m_h;

	word m_positionFillWord, m_positionFillByte;
	longword m_SizeData;
	word m_ChunkBuffer[CHUNK_BYTES];

	void processFillChunk();
	void finishFillChunk();

	inline word rightRotate(word w, unsigned int n);

	void update(const byte *buffer, size_t bufferSize);
	void finish(std::vector<word> &result);
public:
	SHA256();
	~SHA256() { };

	void hash(std::istream &stream, std::vector<word> &result);
};
