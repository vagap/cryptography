#include "Vernam.hpp"

#include "RSA.hpp"

Vernam::Vernam():
	m_key(Crypto::GenerateNumber(32))
{}

Vernam::Vernam(const char fileName[]):
	m_key(0)
{
	std::ifstream fileStream(fileName);
	readSecretKey(fileStream);
}

uint32_t Vernam::coding(uint32_t data)
{
	return data ^ m_key;
}

uint32_t Vernam::decoding(uint32_t data)
{
	return data ^ m_key;
}

void Vernam::writeSecretKey(std::ofstream &keyStream)
{
	keyStream.write((char *) &m_key, sizeof(m_key));
}

void Vernam::readSecretKey(std::ifstream &keyStream)
{
	keyStream.read((char *) &m_key, sizeof(m_key));
}
