#include "ElGamal.hpp"

ElGamal::ElGamal():
	m_p(0), m_cb(0), m_db(0), m_k(0), m_r(0)
{
	uint32_t g, q;

	do{
		q = Crypto::GenerateNumber(24);
	} while (!Crypto::IsPrimeNumber(m_p = (2 * q + 1)));

	while (Crypto::QuickPow(g = Crypto::GenerateNumber(16) % m_p, q, m_p) == 1);

	m_cb = Crypto::GenerateNumber() % (m_p - 1) + 1;
	m_db = Crypto::QuickPow(g, m_cb, m_p);
	m_k = Crypto::GenerateNumber() % (m_p - 2) + 1;
	m_r = Crypto::QuickPow(g, m_k, m_p);
}

uint32_t ElGamal::coding(uint32_t data)
{
	return data * Crypto::QuickPow(m_db, m_k, m_p) % m_p;
}

uint32_t ElGamal::decoding(uint32_t data)
{
	return data * Crypto::QuickPow(m_r, m_p - m_cb - 1, m_p) % m_p;
}

void ElGamal::writePrivateKey(std::ostream &keyStream)
{
	keyStream.write((char *) &m_cb, sizeof(m_cb));
}

void ElGamal::readPrivateKey(std::istream &keyStream)
{
	keyStream.read((char *) &m_cb, sizeof(m_cb));
}

void ElGamal::writePublicKey(std::ostream &keyStream)
{
	keyStream.write((char *) &m_k, sizeof(m_k));
	keyStream.write((char *) &m_p, sizeof(m_p));
	keyStream.write((char *) &m_r, sizeof(m_r));
	keyStream.write((char *) &m_db, sizeof(m_db));
}

void ElGamal::readPublicKey(std::istream &keyStream)
{
	keyStream.read((char *) &m_k, sizeof(m_k));
	keyStream.read((char *) &m_p, sizeof(m_p));
	keyStream.read((char *) &m_r, sizeof(m_r));
	keyStream.read((char *) &m_db, sizeof(m_db));
}
