#pragma once

#include<memory>
#include<sstream>

#include "EncryptStream.hpp"

class SignedFile: public EncryptStream
{
private:
	SignedFile();
	void getSHA(const std::string &signedFileName, std::stringstream &hashStream);

	const static std::string FilePublicKey;
public:
	SignedFile(std::shared_ptr<AbstractEncryptMethod> Encrypt);
	~SignedFile() { };

	void getSigned(const std::string &signedFileName);
	bool isCheckSign(const std::string &signedFileName);
};
