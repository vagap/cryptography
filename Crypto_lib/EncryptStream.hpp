#pragma once

#include<memory>

#include "AbstractEncryptMethod.hpp"

class EncryptStream
{
private:
	std::shared_ptr<AbstractEncryptMethod> m_Encrypt;

	EncryptStream();
protected:
	EncryptStream(std::shared_ptr<AbstractEncryptMethod> Encrypt):	m_Encrypt(Encrypt)
		 { };
	virtual ~EncryptStream() { };

	void writePrivateKey(std::ostream &keyStream);
	void readPrivateKey(std::istream &keyStream);

	void writePublicKey(std::ostream &keyStream);
	void readPublicKey(std::istream &keyStream);

	void codingStream(std::istream &inputStream, std::ostream &outputStream);
	void decodingStream(std::istream &inputStream, std::ostream &outputStream);

};
