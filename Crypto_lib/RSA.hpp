#pragma once

#include "AbstractEncryptMethod.hpp"

class RSA: public AbstractEncryptMethod
{
private:
	int32_t m_c;
	uint32_t m_d, m_n;
public:
	RSA();

	virtual uint32_t coding(uint32_t data);
	virtual uint32_t decoding(uint32_t data);

	virtual void writePrivateKey(std::ostream &keyStream);
	virtual void readPrivateKey(std::istream &keyStream);
	virtual void writePublicKey(std::ostream &keyStream);
	virtual void readPublicKey(std::istream &keyStream);
};
