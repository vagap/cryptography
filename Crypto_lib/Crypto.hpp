#pragma once

#include <cstdint>

typedef enum { ELGAMAL_METHOD, RSA_METHOD } EncryptMethod;

class Crypto
{
private:
	Crypto();

public:
	static uint64_t QuickPow(uint32_t number, uint32_t degree, uint32_t remainder);
	static uint32_t GreatestCommonDivisor(int64_t number1, int64_t number2);
	static uint32_t GreatestCommonDivisor(int64_t number1, int64_t number2, int32_t &coefficient1);
	static bool IsPrimeNumber(uint32_t number);
	static uint32_t GenerateNumber(uint8_t NumberBits = 32);
	static uint32_t GeneratePrimeNumber(uint8_t NumberBits = 32);
};
