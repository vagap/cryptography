#include "EncryptStream.hpp"

void EncryptStream::writePrivateKey(std::ostream &keyStream)
{
	m_Encrypt->writePrivateKey(keyStream);
}

void EncryptStream::readPrivateKey(std::istream &keyStream)
{
	m_Encrypt->readPrivateKey(keyStream);
}

void EncryptStream::writePublicKey(std::ostream &keyStream)
{
	m_Encrypt->writePublicKey(keyStream);
}

void EncryptStream::readPublicKey(std::istream &keyStream)
{
	m_Encrypt->readPublicKey(keyStream);
}

void EncryptStream::codingStream(std::istream &inputStream, std::ostream &outputStream)
{
	std::size_t streamSize;
	inputStream.seekg(0, std::ios::end);
	streamSize = inputStream.tellg();
	inputStream.seekg(0, std::ios::beg);

	outputStream.write((char *) &streamSize, sizeof(streamSize));

	uint32_t Buffer;
	for (std::size_t fileBlock = 0; fileBlock < (streamSize / 3); ++fileBlock) {
		Buffer = 0;
		inputStream.read((char *) &Buffer, 3);
		Buffer = m_Encrypt->coding(Buffer);
		outputStream.write((char *) &Buffer, 4);
	}

	if (streamSize % 3) {
		Buffer = 0;
		inputStream.read((char *) &Buffer, streamSize % 3);
		Buffer = m_Encrypt->coding(Buffer);
		outputStream.write((char *) &Buffer, 4);
	}
}

void EncryptStream::decodingStream(std::istream &inputStream, std::ostream &outputStream)
{
	std::size_t streamSize;
	inputStream.read((char *) &streamSize, sizeof(streamSize));

	uint32_t Buffer = 0;
	for (std::size_t fileBlock = 0; fileBlock < (streamSize / 3); ++fileBlock) {
		inputStream.read((char *) &Buffer, 4);
		Buffer = m_Encrypt->decoding(Buffer);
		outputStream.write((char *) &Buffer, 3);
	}

	if (streamSize % 3) {
		Buffer = 0;
		inputStream.read((char *) &Buffer, 4);
		Buffer = m_Encrypt->decoding(Buffer);
		outputStream.write((char *) &Buffer, streamSize % 3);
	}
}


