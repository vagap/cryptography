#pragma once

#include "EncryptStream.hpp"

class EncryptFile: public EncryptStream
{
private:
	EncryptFile();

	const static std::string FilePublicKey, FilePrivateKey;
public:
	EncryptFile(std::shared_ptr<AbstractEncryptMethod> Encrypt);
	~EncryptFile() { };

	void codingFile(const std::string &inFileName, const std::string &outFileName);
	void decodingFile(const std::string &inFileName, const std::string &outFileName);
};
