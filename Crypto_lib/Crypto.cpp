#include "Crypto.hpp"

#include <cmath>
#include <random>
#include <chrono>

uint64_t Crypto::QuickPow(uint32_t number, uint32_t degree, uint32_t remainder)
{
	uint64_t result = 1;
	while (degree) {
		if (degree & 1)
			result = (result * (uint64_t)number) % (uint64_t)remainder;
		number = ((uint64_t)number * (uint64_t)number) % (uint64_t)remainder;
		degree >>= 1;
	}
	return result;
}

uint32_t Crypto::GreatestCommonDivisor(int64_t number1, int64_t number2)
{
	int32_t dummyVariable;
	return GreatestCommonDivisor(number1, number2, dummyVariable);
}

uint32_t Crypto::GreatestCommonDivisor(int64_t number1, int64_t number2, int32_t &coefficient1)
{
	int64_t matrix[2][3] = { {number1, 1, 0}, {number2, 0, 1} };
	int32_t row1, row2;

	if (matrix[0][0] > matrix[1][0]) {
		row1 = 0;
		row2 = 1;
	} else {
		row1 = 1;
		row2 = 0;
	}

	do {
        for (int32_t i = 0, coef = matrix[row1][0] / matrix[row2][0]; i < 3; ++i)
			matrix[row1][i] -= coef * matrix[row2][i];

		if (matrix[0][0] > matrix[1][0]) {
			row1 = 0;
			row2 = 1;
		} else {
			row1 = 1;
			row2 = 0;
		}
		if (matrix[row2][0] == 0)
			break;
	} while (matrix[row1][0] % matrix[row2][0]);

	coefficient1 = matrix[row2][1];
	return matrix[row2][0];
}

bool Crypto::IsPrimeNumber(uint32_t number)
{
	static const uint32_t PrimeTableSize = 25;
	static const uint32_t PrimeTable[PrimeTableSize] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};

	for (uint32_t i = 0; (i < PrimeTableSize) && (PrimeTable[i] < number); ++i)
		if ((number % PrimeTable[i]) == 0)
			return false;

	uint32_t MaxDivider = sqrt((float)number);
	for (uint32_t i = PrimeTable[PrimeTableSize - 1] + 2; i <= MaxDivider; i += 2)
		if (number % i == 0)
			return false;

	return true;
}

uint32_t Crypto::GenerateNumber(uint8_t NumberBits)
{
	static std::mt19937 generator(std::chrono::system_clock::now().time_since_epoch().count());
	return (generator() & ((1 << (NumberBits - 1)) - 1)) | (1 << (NumberBits - 1));
}

uint32_t Crypto::GeneratePrimeNumber(uint8_t NumberBits)
{
	uint64_t result;
	while (!IsPrimeNumber(result = GenerateNumber(NumberBits)));
	return result;
}
