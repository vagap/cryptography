#include <iostream>

#include "Crypto.hpp"

using namespace std;

int main()
{
	int a, x, p;
	cin >> a >> x >> p;
    cout << "Result:" << Crypto::QuickPow(a, x, p) << endl;
    return 0;
}
